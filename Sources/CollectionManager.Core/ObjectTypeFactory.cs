﻿using System;
using CollectionManager.Contracts.Interfaces;
using CollectionManager.Contracts.Types;
using CollectionManager.Contracts.Types.CollectionObjectTypes;
using CollectionManager.Contracts.Types.CollectionObjectTypes.TelephoneCard;

namespace CollectionManager.Core
{
    public static class ObjectTypeFactory
    {
        /// <summary>
        /// Creates the object using its object type.
        /// </summary>
        /// <param name="objectTypeGuid">The string containing the required guid.</param>
        /// <returns>Returns newly created object.</returns>
        /// <exception cref="ArgumentException">Throws an exception when the guid is not recognized.</exception>
        public static ICollectionObject Create(string objectTypeGuid)
        {
            switch (objectTypeGuid)
            {
                case Constants.CollectionObjectTypeGuid:
                    return new CollectionObject();
                case Constants.TelephoneCardTypeGuid:
                    return new TelephoneCard();
                default:
                    throw new ArgumentException("Unknown object type");
            }
        }
    }
}
