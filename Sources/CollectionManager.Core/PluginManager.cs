﻿using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Integration.Mef;
using CollectionManager.Contracts.Interfaces;

namespace CollectionManager.Core
{
    public class PluginManager
    {
        public const string TitleKey = "Title";

        private static IContainer Container { get; set; }

        public PluginManager()
        {
            var builder = new ContainerBuilder();
            var currentPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var catalog = new DirectoryCatalog(currentPath);
            builder.RegisterComposablePartCatalog(catalog);

            Container = builder.Build();
        }

        public List<string> GetAllNames()
        {
            var res = new List<string>();
            using (var scope = Container.BeginLifetimeScope())
            {
                var c = scope.ResolveExports<ICollectionObject>();
                foreach (var export in c)
                {
                    if (!export.Metadata.ContainsKey(TitleKey))
                    {
                        continue;
                    }
                    var ex = export.Metadata.FirstOrDefault(o => o.Key == TitleKey);
                    res.Add(ex.Value.ToString());
                }
            }

            return res;
        }
    }
}
