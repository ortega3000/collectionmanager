﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using CollectionManager.Contracts;
using CollectionManager.Contracts.Types;
using CollectionManager.Contracts.Types.CollectionObjectTypes;
using CollectionManager.Contracts.Types.CollectionObjectTypes.TelephoneCard;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CollectionManager.Tests.CollectionObjectsTests
{
    [TestClass]
    public class TelephoneCardsTests : TestsBase
    {
        [TestMethod]
        public void CanAddItem()
        {
            var newItem = AddOneTelephoneCard(2019, 10, Enums.Currency.UAH);

            Assert.IsTrue(Db.TryFindItemById(TestCollectionName, newItem.ObjectId, out var item));
            Assert.IsNotNull(item);

            var card = new TelephoneCard(item);
            Assert.IsNotNull(card);

            Assert.IsTrue(newItem.Equals(card));
        }

        [TestMethod]
        public void CanLoadImage()
        {
            var card = new TelephoneCard();
            var imagePath = Path.Combine(GetTestDataPath(), "one_pixel.jpg");
            Assert.IsTrue(card.LoadFromImage(imagePath, ImageType.Back));
            Assert.IsTrue(card.LoadFromImage(imagePath, ImageType.Front));

            Assert.AreEqual(2, card.Images.Count);
            Assert.AreEqual(2, card.BackImage.OriginalSize.Width);
            Assert.AreEqual(1, card.BackImage.OriginalSize.Height);
            Assert.AreEqual(2, card.FrontImage.OriginalSize.Width);
            Assert.AreEqual(1, card.FrontImage.OriginalSize.Height);
        }

        private TelephoneCard AddOneTelephoneCard(int year, int value, Enums.Currency currency)
        {
            var itemToAdd = new TelephoneCard
            {
                Active = true,
                Year = year,
                Value = value,
                Currency = currency,
                Title = Title1,
                Images = new List<ObjectImage>()
                {
                    new ObjectImage
                    {
                        Content = new byte[] {0x00, 0x01, 0x02},
                        Title = "Back side",
                        OriginalFileName = "back.jpg",
                        OriginalSize = new ImageSize(100,50),
                        ImageType = ImageType.Back
                    },
                    new ObjectImage
                    {
                        Content = new byte[] {0x10, 0x11, 0x12},
                        Title = "Front side",
                        OriginalFileName = "front.jpg",
                        OriginalSize = new ImageSize(101, 51),
                        ImageType = ImageType.Front
                    }
                }
            };

            Assert.AreEqual(year, itemToAdd.Year);
            Assert.AreEqual(Title1, itemToAdd.Title);
            Assert.AreEqual(value, itemToAdd.Value);
            Assert.AreEqual(currency, itemToAdd.Currency);
            Assert.IsTrue(itemToAdd.Active);

            Assert.IsTrue(Db.AddItemToCollection(TestCollectionName, itemToAdd));

            Assert.IsTrue(Db.TryFindItemById(TestCollectionName, itemToAdd.ObjectId, out var item));
            Assert.IsNotNull(item);
            Assert.IsTrue(itemToAdd.Equals(item));

            return itemToAdd;
        }
    }
}
