﻿using System;
using System.IO;
using System.Reflection;
using System.Threading;
using CollectionManager.Contracts.Interfaces;
using CollectionManager.Contracts.Types;
using CollectionManager.Contracts.Types.CollectionObjectTypes;
using CollectionManager.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CollectionManager.Tests
{
    public class TestsBase
    {
        protected string ConnectionString = @"mongodb://localhost";
        protected CancellationTokenSource CancellationTokenSource = new CancellationTokenSource();
        protected string TestDatabaseName = "test_database";
        protected string TestCollectionName = "test_collection";

        protected string Title1 = "title1";
        protected string Title2 = "title2";

        protected string GetTestDataPath()
        {
            var dir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Assert.IsNotNull(dir);

            var result = Path.Combine(dir, "TestData");
            Assert.IsTrue(Directory.Exists(result));

            return result;
        }

        protected MongoDatabase Db;

        protected void CreateDefaultCollection()
        {
            Db.UseDatabase(TestDatabaseName);
            Assert.IsTrue(Db.CreateCollection(TestCollectionName));
        }

        protected ICollectionObject AddOneItem(string title)
        {
            var itemToAdd = new CollectionObject
            {
                Active = true,
                Archived = false,
                DateAdded = DateTime.UtcNow,
                Title = title,
                Description = string.Empty
            };
            Assert.IsTrue(Db.AddItemToCollection(TestCollectionName, itemToAdd));

            Assert.IsTrue(Db.TryFindItemById(TestCollectionName, itemToAdd.ObjectId, out var item));
            Assert.IsNotNull(item);

            return itemToAdd;
        }

        [TestInitialize]
        public void InitTests()
        {
            Db = new MongoDatabase(ConnectionString, CancellationTokenSource.Token);
            Db.DropDatabase(TestDatabaseName);
            Db.UseDatabase(TestDatabaseName);
        }



    }
}