﻿using System;
using System.Threading;
using CollectionManager.Contracts.Interfaces;
using CollectionManager.Contracts.Types;
using CollectionManager.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CollectionManager.Tests
{
    [TestClass]
    public class DatabaseTests : TestsBase
    {
        [TestMethod]
        public void CanCreateCollection()
        {
            Assert.IsFalse(Db.DoesCollectionExist(TestCollectionName));

            Assert.IsTrue(Db.CreateCollection(TestCollectionName));
            Assert.IsTrue(Db.DoesCollectionExist(TestCollectionName));
        }

        [TestMethod]
        public void CanDropCollection()
        {
            Assert.IsFalse(Db.DoesCollectionExist(TestCollectionName));

            Assert.IsTrue(Db.CreateCollection(TestCollectionName));
            Assert.IsTrue(Db.DoesCollectionExist(TestCollectionName));

            Assert.IsTrue(Db.DropCollection(TestCollectionName));
            Assert.IsFalse(Db.DoesCollectionExist(TestCollectionName));
        }

        [TestMethod]
        public void CanExportCollection()
        {
            Assert.IsFalse(Db.DoesCollectionExist(TestCollectionName));

            Assert.IsTrue(Db.CreateCollection(TestCollectionName));
            Assert.IsTrue(Db.DoesCollectionExist(TestCollectionName));

            Assert.IsTrue(Db.DropCollection(TestCollectionName));
            Assert.IsFalse(Db.DoesCollectionExist(TestCollectionName));
        }

        [TestMethod]
        public void CanAddItem()
        {
            var newItem = AddOneItem(Title1);

            Assert.IsTrue(Db.TryFindItemById(TestCollectionName, newItem.ObjectId, out var item));
            Assert.IsNotNull(item);
            Assert.AreEqual(Title1, item.Title);
        }

        [TestMethod]
        public void CanUpdateItem()
        {
            var newItem = AddOneItem(Title1);

            Assert.IsTrue(Db.TryFindItemById(TestCollectionName, newItem.ObjectId, out var item));
            Assert.IsNotNull(item);

            item.Title = Title2;
            Assert.IsTrue(Db.UpdateItem(TestCollectionName, item));
            Assert.IsTrue(Db.TryFindItemById(TestCollectionName, newItem.ObjectId, out item));
            Assert.IsNotNull(item);
        }

        [TestMethod]
        public void CanDeleteItem()
        {
            var newItem = AddOneItem(Title1);

            Assert.IsTrue(Db.TryDropItemById(TestCollectionName, newItem.ObjectId));

            Assert.IsFalse(Db.TryFindItemById(TestCollectionName, newItem.ObjectId, out var item));
            Assert.IsNull(item);
        }
    }
}
