﻿using System;
using System.Drawing;
using CollectionManager.Contracts.Types;
using CollectionManager.Contracts.Types.CollectionObjectTypes;
using CollectionManager.Contracts.Types.CollectionObjectTypes.TelephoneCard;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CollectionManager.Tests
{
    [TestClass]
    public class ComparisonTests
    {
        [TestMethod]
        public void ImageSizesAreEqual()
        {
            var imageSize1 = new ImageSize(1, 2);
            var imageSize2 = new ImageSize(1, 2);

            Assert.IsTrue(imageSize1.Equals(imageSize2));
        }

        [TestMethod]
        public void ImageSizesAreNotEqual()
        {
            var imageSize1 = new ImageSize(1, 2);
            var imageSize2 = new ImageSize(10, 20);

            Assert.IsFalse(imageSize1.Equals(imageSize2));
        }

        [TestMethod]
        public void ObjectImagesAreEqual()
        {
            var card1 = new ObjectImages
            {
                Back = new ObjectImage
                {
                    Content = new byte[] {0x01, 0x02},
                    Title = "b",
                    OriginalFileName = "ofn",
                    OriginalSize = new ImageSize(1, 2)
                },
                Front = new ObjectImage
                {
                    Content = new byte[] {0x11, 0x12},
                    Title = "b1",
                    OriginalFileName = "ofn1",
                    OriginalSize = new ImageSize(11, 21)
                }
            };
            var card2 = new ObjectImages
            {
                Back = new ObjectImage
                {
                    Content = new byte[] { 0x01, 0x02 },
                    Title = "b",
                    OriginalFileName = "ofn",
                    OriginalSize = new ImageSize(1, 2)
                },
                Front = new ObjectImage
                {
                    Content = new byte[] { 0x11, 0x12 },
                    Title = "b1",
                    OriginalFileName = "ofn1",
                    OriginalSize = new ImageSize(11, 21)
                }
            };

            Assert.IsTrue(card1.Equals(card2));
        }

        [TestMethod]
        public void ObjectImagesAreNotEqual_FrontIsNull()
        {
            var card1 = new ObjectImages
            {
                Back = new ObjectImage
                {
                    Content = new byte[] {0x01, 0x02},
                    Title = "b",
                    OriginalFileName = "ofn",
                    OriginalSize = new ImageSize(1, 2)
                },
                Front = new ObjectImage
                {
                    Content = new byte[] {0x11, 0x12},
                    Title = "b1",
                    OriginalFileName = "ofn1",
                    OriginalSize = new ImageSize(11, 21)
                }
            };
            var card2 = new ObjectImages
            {
                Back = new ObjectImage
                {
                    Content = new byte[] { 0x01, 0x02 },
                    Title = "b",
                    OriginalFileName = "ofn",
                    OriginalSize = new ImageSize(1, 2)
                },
                Front = null
            };

            Assert.IsFalse(card1.Equals(card2));
        }

        [TestMethod]
        public void ObjectImagesAreNotEqual_BackIsNull()
        {
            var card1 = new ObjectImages
            {
                Back = new ObjectImage
                {
                    Content = new byte[] {0x01, 0x02},
                    Title = "b",
                    OriginalFileName = "ofn",
                    OriginalSize = new ImageSize(1, 2)
                },
                Front = new ObjectImage
                {
                    Content = new byte[] {0x11, 0x12},
                    Title = "b1",
                    OriginalFileName = "ofn1",
                    OriginalSize = new ImageSize(11, 21)
                }
            };
            var card2 = new ObjectImages
            {
                Back = null,
                Front = new ObjectImage
                {
                    Content = new byte[] { 0x11, 0x12 },
                    Title = "b1",
                    OriginalFileName = "ofn1",
                    OriginalSize = new ImageSize(11, 21)
                }
            };

            Assert.IsFalse(card1.Equals(card2));
        }

        [TestMethod]
        public void ObjectImagesAreNotEqual_FrontContentDiffers()
        {
            var card1 = new ObjectImages
            {
                Back = new ObjectImage
                {
                    Content = new byte[] {0x01, 0x02},
                    Title = "b",
                    OriginalFileName = "ofn",
                    OriginalSize = new ImageSize(1, 2)
                },
                Front = new ObjectImage
                {
                    Content = new byte[] {0x11, 0x12},
                    Title = "b1",
                    OriginalFileName = "ofn1",
                    OriginalSize = new ImageSize(11, 21)
                }
            };
            var card2 = new ObjectImages
            {
                Back = new ObjectImage
                {
                    Content = new byte[] { 0x01, 0x02 },
                    Title = "b",
                    OriginalFileName = "ofn",
                    OriginalSize = new ImageSize(1, 2)
                },
                Front = new ObjectImage
                {
                    Content = new byte[] { 0x11, 0x12, 0x13 },
                    Title = "b1",
                    OriginalFileName = "ofn1",
                    OriginalSize = new ImageSize(11, 21)
                }
            };

            Assert.IsFalse(card1.Equals(card2));
        }

        [TestMethod]
        public void ObjectImagesAreNotEqual_BackContentDiffers()
        {
            var card1 = new ObjectImages
            {
                Back = new ObjectImage
                {
                    Content = new byte[] {0x01, 0x02},
                    Title = "b",
                    OriginalFileName = "ofn",
                    OriginalSize = new ImageSize(1, 2)
                },
                Front = new ObjectImage
                {
                    Content = new byte[] {0x11, 0x12},
                    Title = "b1",
                    OriginalFileName = "ofn1",
                    OriginalSize = new ImageSize(11, 21)
                }
            };
            var card2 = new ObjectImages
            {
                Back = new ObjectImage
                {
                    Content = new byte[] { 0x01, 0x02, 0x03 },
                    Title = "b",
                    OriginalFileName = "ofn",
                    OriginalSize = new ImageSize(1, 2)
                },
                Front = new ObjectImage
                {
                    Content = new byte[] { 0x11, 0x12 },
                    Title = "b1",
                    OriginalFileName = "ofn1",
                    OriginalSize = new ImageSize(11, 21)
                }
            };

            Assert.IsFalse(card1.Equals(card2));
        }
    }
}
