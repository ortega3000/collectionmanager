﻿using System;
using System.Threading;
using CollectionManager.Contracts.Extensions;
using CollectionManager.Contracts.Types;
using CollectionManager.Contracts.Types.CollectionObjectTypes;
using CollectionManager.Database;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Driver;

namespace CollectionManager.Tests
{
    [TestClass]
    public class CollectionsTests : TestsBase
    {
        [TestMethod]
        public void CanSerializeOneCollection()
        {
            Assert.IsTrue(Db.CreateCollection(TestCollectionName));

            AddOneItem(Title1);

            var client = new MongoClient(ConnectionString);
            var database = client.GetDatabase(TestDatabaseName);
            var collection = database.GetCollection<CollectionObject>(TestCollectionName);
            Assert.IsNotNull(collection);

            var stringView = collection.ConvertToString();
            Assert.IsFalse(string.IsNullOrEmpty(stringView));
        }
    }
}
