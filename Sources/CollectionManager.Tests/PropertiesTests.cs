﻿using System;
using System.Collections.Generic;
using CollectionManager.Contracts.Interfaces;
using CollectionManager.Contracts.Types;
using CollectionManager.Contracts.Types.CollectionObjectTypes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CollectionManager.Tests
{
    [TestClass]
    public class PropertiesTests
    {
        private Property CreateTestProperty()
        {
            var property = new Property(CollectionObject.TitlePropertyId, PropertyType.String,
                CollectionObject.TitlePropertyTitle, "Test");

            return property;
        }

        [TestMethod]
        public void CanAddSubProperty()
        {
            var propertyToTest = CreateTestProperty();

            var subProperty = CreateTestProperty();
            propertyToTest.AddSubProperty(subProperty);
            Assert.AreEqual(1, propertyToTest.SubProperties.Count);

            Property property;
            Assert.IsTrue(propertyToTest.SubProperties.TryFindPropertyById(subProperty.PropertyId, out property));
            Assert.IsNotNull(property);
            Assert.AreEqual(subProperty.PropertyId, property.PropertyId);

            var subProperty2 = CreateTestProperty();
            subProperty.AddSubProperty(subProperty2);

            Assert.IsTrue(propertyToTest.SubProperties.TryFindPropertyById(subProperty2.PropertyId, out property));
            Assert.IsNotNull(property);
            Assert.AreEqual(subProperty2.PropertyId, property.PropertyId);
        }

        [TestMethod]
        public void CanRemoveSubProperty()
        {
            var property = CreateTestProperty();

            var subProperty = CreateTestProperty();
            property.AddSubProperty(subProperty);
            Assert.AreEqual(1, property.SubProperties.Count);

            property.RemoveSubProperty(subProperty.PropertyId);
            Assert.AreEqual(0, property.SubProperties.Count);
        }

        [TestMethod]
        public void CanClearSubProperties()
        {
            var property = CreateTestProperty();

            var subProperty = CreateTestProperty();
            property.AddSubProperty(subProperty);
            Assert.AreEqual(1, property.SubProperties.Count);

            property.ClearSubProperties();
            Assert.AreEqual(0, property.SubProperties.Count);
        }
    }
}
