﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CollectionManager.Contracts.GUI.PhoneCards;
using CollectionManager.Contracts.Types.CollectionObjectTypes.TelephoneCard;

namespace CollectionManager.Contracts.Types
{
    public static class CollectionTypes
    {
        public enum CollectionType
        {
            PhoneCard
        }

        public class CollectionTypeInfo
        {
            public CollectionType CollectionType { get; set; }
            public string DisplayName { get; set; }
            public Type TypeOfObject { get; set; }
            public Type CollectionListViewType { get; set; }
        }

        public static List<CollectionTypeInfo> KnownCollections = new List<CollectionTypeInfo>
        {
            new CollectionTypeInfo
            {
                CollectionType = CollectionType.PhoneCard,
                DisplayName = "Phone card",
                TypeOfObject = typeof(TelephoneCard),
                CollectionListViewType = typeof(TelephoneCardsList)
            }
        };
    }
}
