﻿using System;

namespace CollectionManager.Contracts.Types
{
    [Serializable]
    public class ImageSize
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public ImageSize(int w, int h)
        {
            Width = w;
            Height = h;
        }
    }
}