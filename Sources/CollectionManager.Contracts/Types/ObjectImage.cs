﻿using System.Diagnostics;
using System.Drawing;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CollectionManager.Contracts.Types
{
    public class ObjectImage
    {
        public ImageType ImageType { get; set; }
        public string Title { get; set; }
        public string OriginalFileName { get; set; }
        public ImageSize OriginalSize { get; set; }
        public byte[] Content { get; set; }

        public void LoadFromFile(string filePath)
        {
            Debug.Assert(File.Exists(filePath), $"{filePath} does not exist.");

            Content = File.ReadAllBytes(filePath);
            OriginalFileName = Path.GetFileName(filePath);
        }

        public override bool Equals(object obj)
        {
            var item = obj as ObjectImage;
            if (item == null)
            {
                return false;
            }

            if (Title != item.Title 
            || OriginalFileName != item.OriginalFileName
            || !OriginalSize.Equals(item.OriginalSize)
            || Content.Length != item.Content.Length)
            {
                return false;
            }

            for (int i = 0; i < Content.Length; i++)
            {
                if (Content[i] != item.Content[i])
                {
                    return false;
                }
            }

            return true;
        }

        public override int GetHashCode()
        {
            return 0;
        }
    }

    public enum ImageType
    {
        Front,
        Back,
        Top,
        Bottom,
        Left,
        Right,
        Other
    }
}