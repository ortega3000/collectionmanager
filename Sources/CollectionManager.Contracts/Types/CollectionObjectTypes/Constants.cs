﻿using System;

namespace CollectionManager.Contracts.Types.CollectionObjectTypes
{
    public class Constants
    {
        public const string TitleKey = "Title";
        public const string Value = "Value";
        public const string Country = "Country";

        public const string CollectionObjectTypeGuid = "{C41ED9A8-78AA-49F0-AC3E-B7B0E432120D}";
        public const string TelephoneCardTypeGuid = "{C7A712BC-4255-4B7F-BCDD-63B1D4F8B3D4}";

        public static string MyDatabaseName = "my_collection";
    }
}