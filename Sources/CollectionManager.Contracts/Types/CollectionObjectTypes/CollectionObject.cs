﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using CollectionManager.Contracts.Interfaces;
using CollectionManager.Contracts.Types.CollectionObjectTypes.TelephoneCard;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace CollectionManager.Contracts.Types.CollectionObjectTypes
{
    [BsonIgnoreExtraElements]
    public class CollectionObject : ICollectionObject
    {
        public static readonly Guid TitlePropertyId = new Guid("{29684348-6FE2-4F60-8432-CA4344365C4B}");
        public static readonly Guid DescriptionPropertyId = new Guid("{C44D25C2-55E4-40BA-B05F-A39EF4DAF824}");
        public static readonly Guid DateAddedPropertyId = new Guid("{6E6DEEA7-D26D-4BF3-83EA-7FECC033F430}");
        public static readonly Guid ActivePropertyId = new Guid("{AB0C6B24-A985-4EEE-B590-FDF0DAD1EC7F}");
        public static readonly Guid ArchivedPropertyId = new Guid("{AF1B4569-F0D7-4AF8-8D26-3231BADE6F96}");
        public static readonly Guid TagsPropertyId = new Guid("{B08DF9F4-4004-46D9-8F5C-C957CA06851C}");

        public const string TitlePropertyTitle = "Title";
        public const string DescriptionPropertyTitle = "Description";
        public const string DateAddedPropertyTitle = "Date added";
        public const string ActivePropertyTitle = "Active";
        public const string ArchivedPropertyTitle = "Archived";
        public const string TagsPropertyTitle = "Tags";

        public ObjectId Id { get; set; }
        public Guid ObjectId { get; set; }
        public Guid ObjectType { get; set; }
        public string Title
        {
            get => GetProperty(TitlePropertyId, string.Empty);
            set => UpdateProperty(TitlePropertyId, value);
        }

        public string Description
        {
            get => GetProperty(DescriptionPropertyId, string.Empty);
            set => UpdateProperty(DescriptionPropertyId, value);
        }

        public DateTime DateAdded
        {
            get => GetProperty(DateAddedPropertyId, DateTime.MinValue);
            set => UpdateProperty(DateAddedPropertyId, value);
        }
        public bool Active
        {
            get => GetProperty(ActivePropertyId, false);
            set => UpdateProperty(ActivePropertyId, value);
        }
        public bool Archived
        {
            get => GetProperty(ArchivedPropertyId, false);
            set => UpdateProperty(ArchivedPropertyId, value);
        }
        public List<string> Tags
        {
            get => GetListProperty<string>(TagsPropertyId);
            set => UpdateListProperty(TagsPropertyId, value);
        }
        public List<ObjectImage> Images = new List<ObjectImage>();

        public PropertiesCollection Properties { get; set; }

        protected virtual void InitiateProperties()
        {
            Properties = new PropertiesCollection
            {
                new Property(TitlePropertyId, PropertyType.String, TitlePropertyTitle, string.Empty),
                new Property(DescriptionPropertyId, PropertyType.String, DescriptionPropertyTitle, string.Empty),
                new Property(DateAddedPropertyId, PropertyType.DateTime, DateAddedPropertyTitle,
                    DateTime.MinValue),
                new Property(ArchivedPropertyId, PropertyType.Bool, ArchivedPropertyTitle, false),
                new Property(ActivePropertyId, PropertyType.Bool, ActivePropertyTitle, false),
                new Property(TagsPropertyId, PropertyType.Collection, TagsPropertyTitle, string.Empty)
            };
        }

        public void UpdateProperty<T>(Guid id, T value)
        {
            Property property;
            if (Properties.TryFindPropertyById(id, out property))
            {
                property.Value = value;
            }
        }

        public void UpdateListProperty<T>(Guid id, List<T> value)
        {
            Property property;
            if (Properties.TryFindPropertyById(id, out property))
            {
                property.SubProperties = new PropertiesCollection();
                foreach (var item in value)
                {
                    property.SubProperties.Add(new Property(Guid.NewGuid(), PropertyType.String, string.Empty, item));
                }
            }
        }

        public void AddProperty(Property property)
        {
            Properties.TryAddProperty(property);
        }

        public bool TryAddProperty(Guid parentId, Property property)
        {
            Property parentProperty;
            if (Properties.TryFindPropertyById(parentId, out parentProperty))
            {
                parentProperty.AddSubProperty(property);
                return true;
            }

            return false;
        }

        public void AddProperty<T>(Guid id, string title, PropertyType type, T value)
        {
            throw new NotImplementedException();
        }

        public T GetProperty<T>(Guid id, T defaultValue)
        {
            if (Properties.TryFindPropertyById(id, out var property))
            {
                return (T)property.Value;
            }

            return defaultValue;
        }

        public List<T> GetListProperty<T>(Guid id)
        {
            var result = new List<T>();

            if (Properties.TryFindPropertyById(id, out var property))
            {
                var subProperties = property.SubProperties;
                foreach (var subProperty in subProperties)
                {
                    result.Add((T)subProperty.Value);
                }

                return result;
            }

            return result;
        }

        public CollectionObject()
        {
            ObjectType = new Guid(Constants.CollectionObjectTypeGuid);

            ObjectId = Guid.NewGuid();
            Properties = new PropertiesCollection();

            InitiateProperties();
        }

        public CollectionObject(Guid objectType)
        {
            ObjectId = Guid.NewGuid();
            Properties = new PropertiesCollection();

            ObjectType = objectType;

            InitiateProperties();
        }

        public bool LoadFromImage(string pathToImage, ImageType imageType)
        {
            if (!File.Exists(pathToImage))
            {
                return false;
            }
            var image = Images.FirstOrDefault(o => o.ImageType == imageType);
            if (image == null)
            {
                image = new ObjectImage
                {
                    OriginalFileName = pathToImage,
                    Title = string.Empty,
                    ImageType = imageType,
                    OriginalSize = new ImageSize(0,0)
                };
                Images.Add(image);
            }

            image.Content = File.ReadAllBytes(pathToImage);

            var img = Image.FromFile(pathToImage);
            image.OriginalSize = new ImageSize(img.Size.Width, img.Size.Height);

            return true;
        }
    }
}
