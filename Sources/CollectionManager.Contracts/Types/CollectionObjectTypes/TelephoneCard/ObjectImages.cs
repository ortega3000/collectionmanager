﻿using System.Linq;

namespace CollectionManager.Contracts.Types.CollectionObjectTypes.TelephoneCard
{
    public class ObjectImages
    {
        public bool HasFrontImage => Front != null && Front.Content.Any();
        public bool HasBackImage => Back != null && Back.Content.Any();

        public ObjectImage Front { get; set; }
        public ObjectImage Back { get; set; }

        public override bool Equals(object obj)
        {
            var item = obj as ObjectImages;
            if (item == null)
            {
                return false;
            }

            if (item.Front == null && Front != null)
            {
                return false;
            }

            if (item.Front != null && Front == null)
            {
                return false;
            }

            if (item.Back == null && Back != null)
            {
                return false;
            }

            if (item.Back != null && Back == null)
            {
                return false;
            }

            return item.Front.Equals(Front) && item.Back.Equals(Back);
        }
    }
}