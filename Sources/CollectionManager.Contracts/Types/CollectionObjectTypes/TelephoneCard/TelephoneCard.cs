﻿using System;
using System.ComponentModel.Composition;
using System.Linq;
using CollectionManager.Contracts.Interfaces;

namespace CollectionManager.Contracts.Types.CollectionObjectTypes.TelephoneCard
{
    [Export(typeof(ICollectionObject))]
    [ExportMetadata(Constants.TitleKey, "Telephone card")]
    public class TelephoneCard : CollectionObject, ICollectionObject
    {
        public static readonly Guid YearPropertyId = new Guid("{86B9F0B3-974B-4282-B8E5-DA37F23AD4EB}");
        public static readonly Guid CountryPropertyId = new Guid("{568C9D88-B1B1-4520-BA80-6E653867AD9B}");
        public static readonly Guid CurrencyPropertyId = new Guid("{458DC813-6772-41CF-BA85-A3C119E5393A}");
        public static readonly Guid ValuePropertyId = new Guid("{2EC4E2BB-5F29-4D6F-9E48-C4AC4C0E6E27}");

        public const string YearPropertyTitle = "Year";
        public const string CountryPropertyTitle = "Country";
        public const string CurrencyPropertyTitle = "Currency";
        public const string ValuePropertyTitle = "Value";

        public ObjectImage FrontImage
        {
            get
            {
                var res = Images.FirstOrDefault(o => o.ImageType == ImageType.Front);
                if (res == null)
                {
                    res = new ObjectImage
                    {
                        ImageType = ImageType.Front
                    };
                    Images.Add(res);
                }

                return res;
            }
            set
            {
                var res = Images.FirstOrDefault(o => o.ImageType == ImageType.Front);
                if (res == null)
                {
                    Images.Add(value);
                }
                else
                {
                    res.ImageType = ImageType.Front;
                    res.Title = value.Title;
                    res.Content = value.Content;
                    res.OriginalFileName = value.OriginalFileName;
                    res.OriginalSize = value.OriginalSize;
                }
            }
        }

        public ObjectImage BackImage
        {
            get
            {
                var res = Images.FirstOrDefault(o => o.ImageType == ImageType.Back);
                if (res == null)
                {
                    res = new ObjectImage
                    {
                        ImageType = ImageType.Back
                    };
                    Images.Add(res);
                }

                return res;
            }
            set
            {
                var res = Images.FirstOrDefault(o => o.ImageType == ImageType.Back);
                if (res == null)
                {
                    Images.Add(value);
                }
                else
                {
                    res.ImageType = ImageType.Back;
                    res.Title = value.Title;
                    res.Content = value.Content;
                    res.OriginalFileName = value.OriginalFileName;
                    res.OriginalSize = value.OriginalSize;
                }
            }
        }

        public TelephoneCard() : base(new Guid(Constants.TelephoneCardTypeGuid))
        {
        }

        public override bool Equals(object obj)
        {
            var item = obj as TelephoneCard;
            if (item == null)
            {
                return false;
            }

            if (item.Active != Active
                || item.Archived != Archived
                || item.Country != Country
                || item.Currency != Currency
                || item.Value != Value
                || item.Year != Year
                || item.Title != Title
                || item.Description != Description
                || item.ObjectType != ObjectType)
            {
                return false;
            }

            if (item.Properties.Count != Properties.Count)
            {
                return false;
            }

            if (!item.Properties.Equals(Properties))
            {
                return false;
            }

            return true;
        }

        public TelephoneCard(ICollectionObject baseObject) 
            : base(new Guid(Constants.TelephoneCardTypeGuid))
        {
            Properties = baseObject.Properties;
        }

        protected override void InitiateProperties()
        {
            base.InitiateProperties();

            Properties.Add(new Property(YearPropertyId, PropertyType.Integer, YearPropertyTitle, 0));
            Properties.Add(new Property(CountryPropertyId, PropertyType.Integer, CountryPropertyTitle, 0));
            Properties.Add(new Property(CurrencyPropertyId, PropertyType.Integer, CurrencyPropertyTitle, 0));
            Properties.Add(new Property(ValuePropertyId, PropertyType.Integer, ValuePropertyTitle, 0));
        }

        public int Year
        {
            get => GetProperty(YearPropertyId, 0);
            set => UpdateProperty(YearPropertyId, value);
        }

        public int Value
        {
            get => GetProperty(ValuePropertyId, 0);
            set => UpdateProperty(ValuePropertyId, value);
        }

        public Enums.Country Country
        {
            get => GetProperty(CountryPropertyId, Enums.Country.Ukraine);
            set => UpdateProperty(CountryPropertyId, value);
        }

        public Enums.Currency Currency
        {
            get => GetProperty(CurrencyPropertyId, Enums.Currency.UAH);
            set => UpdateProperty(CurrencyPropertyId, value);
        }
    }
}
