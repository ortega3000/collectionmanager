﻿namespace CollectionManager.Contracts.Types
{
    public enum PropertyType
    {
        Bool,
        String,
        Date,
        DateTime,
        Integer,
        Double,
        Image,
        Hyperlink,
        Collection
    }
}