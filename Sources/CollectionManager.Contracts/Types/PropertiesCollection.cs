﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CollectionManager.Contracts.Types
{
    public class PropertiesCollection : List<Property>
    {
        public bool TryFindPropertyById(Guid id, out Property property)
        {
            property = null;

            if (this.Count == 0)
            {
                return false;
            }

            foreach (var item in this)
            {
                if (item.PropertyId == id)
                {
                    property = item;
                    return true;
                }

                if (item.SubProperties.TryFindPropertyById(id, out property))
                {
                    return true;
                }
            }

            return false;
        }

        public bool TryAddProperty(Property property)
        {
            if (this.Any(o => o.PropertyId == property.PropertyId))
            {
                return false;
            }

            Add(property);
            return true;
        }

        public override bool Equals(object obj)
        {
            var item = obj as PropertiesCollection;
            if (item == null)
            {
                return false;
            }

            if (item.Count != Count)
            {
                return false;
            }

            for (var i = 0; i < Count; i++)
            {
                if (!item[i].Equals(this[i]))
                {
                    return false;
                }
            }

            return true;
        }

        public override int GetHashCode()
        {
            int hash = 0;
            foreach (var item in this)
            {
                hash ^= item.GetHashCode();
            }

            return hash;
        }
    }
}