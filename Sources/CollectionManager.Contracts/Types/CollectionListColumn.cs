﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionManager.Contracts.Types
{
    public struct CollectionListColumn
    {
        /// <summary>
        /// The width of column in pixels
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// The title of the column
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Identified of the property to be shown in this column
        /// </summary>
        public Guid PropertyId { get; set; }

        public CollectionListColumn(Guid propertyId, int width, string title)
        {
            PropertyId = propertyId;
            Width = width;
            Title = title;
        }
    }

    public class CollectionListColumns : List<CollectionListColumn>
    {
        public void AddColumn(Guid propertyId, int width, string title)
        {
            if (this.Any(o => o.PropertyId == propertyId))
            {
                return;
            }

            this.Add(new CollectionListColumn(propertyId, width, title));
        }
    }
}
