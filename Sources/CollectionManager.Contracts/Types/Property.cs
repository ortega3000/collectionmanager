﻿using System;
using System.Collections.Generic;
using System.Linq;
using CollectionManager.Contracts.Interfaces;

namespace CollectionManager.Contracts.Types
{
    public class Property// : IProperty
    {
        public PropertyType Type { get; set; }
        public Guid PropertyId { get; set; }
        public string Title { get; set; }
        public object Value { get; set; }
        public PropertiesCollection SubProperties { get; set; }

        public Property(Guid propertyId, PropertyType propertyType, string title, object value)
        {
            Title = title;
            Value = value;
            Type = propertyType;
            PropertyId = propertyId;
            SubProperties = new PropertiesCollection();
        }

        public void AddSubProperty(Property property)
        {
            SubProperties.Add(property);
        }

        public void RemoveSubProperty(Guid id)
        {
            var propertyToRemove = SubProperties.FirstOrDefault(o => o.PropertyId == id);
            if (propertyToRemove == null)
            {
                return;
            }

            SubProperties.Remove(propertyToRemove);
        }

        public void ClearSubProperties()
        {
            SubProperties.Clear();
        }

        public override int GetHashCode()
        {
            var result = 0;
            result ^= Title.GetHashCode();
            result ^= PropertyId.GetHashCode();
            result ^= Type.GetHashCode();
            result ^= Value.GetHashCode();
            result ^= SubProperties.GetHashCode();

            return result;
        }

        public override bool Equals(object obj)
        {
            var item = obj as Property;
            if (item == null)
            {
                return false;
            }

            if (Title != item.Title)
            {
                return false;
            }

            if (PropertyId != item.PropertyId)
            {
                return false;
            }

            if (Type != item.Type)
            {
                return false;
            }

            if (Type == PropertyType.String)
            {
                if (Value.ToString() != item.Value.ToString())
                {
                    return false;
                }
            }

            if (!SubProperties.Equals(item.SubProperties))
            {
                return false;
            }

            return true;
        }
    }
}