﻿using System.Windows.Forms;
using CollectionManager.Contracts.GUI.Common;
using CollectionManager.Contracts.Types.CollectionObjectTypes;
using CollectionManager.Contracts.Types.CollectionObjectTypes.TelephoneCard;

namespace CollectionManager.Contracts.GUI.PhoneCards
{
    public partial class TelephoneCardsList : CollectionObjectsList
    {
        public TelephoneCardsList()
        {
            InitializeComponent();
        }

        protected override void InitColumns()
        {
            Columns.Clear();
            Columns.AddColumn(TelephoneCard.YearPropertyId, 50, TelephoneCard.YearPropertyTitle);
            Columns.AddColumn(TelephoneCard.CountryPropertyId, 250, TelephoneCard.CountryPropertyTitle);
            Columns.AddColumn(TelephoneCard.ValuePropertyId, 100, TelephoneCard.ValuePropertyTitle);
            Columns.AddColumn(TelephoneCard.CurrencyPropertyId, 100, TelephoneCard.CurrencyPropertyTitle);
        }
    }
}
