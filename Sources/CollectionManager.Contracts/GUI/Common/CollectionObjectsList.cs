﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CollectionManager.Contracts.Interfaces;
using CollectionManager.Contracts.Types;
using CollectionManager.Contracts.Types.CollectionObjectTypes;

namespace CollectionManager.Contracts.GUI.Common
{
    public partial class CollectionObjectsList : UserControl
    {
        protected CollectionListColumns Columns = new CollectionListColumns();
        private List<ICollectionObject> _collection;

        public List<ICollectionObject> Collection
        {
            get => _collection;
            set
            {
                _collection = value;
                lvCollection.BeginUpdate();

                try
                {
                    lvCollection.Items.Clear();
                    foreach (var item in _collection)
                    {
                        var listItem = new ListViewItem();
                        foreach (var column in Columns)
                        {
                            var propertyValue = string.Empty;
                            if (item.Properties.TryFindPropertyById(column.PropertyId, out var property))
                            {
                                propertyValue = property.Value.ToString();
                            }

                            listItem.SubItems.Add(propertyValue);
                        }
                    }
                }
                finally
                {
                    lvCollection.EndUpdate();
                }
            }
        }

        public CollectionObjectsList()
        {
            InitializeComponent();
        }

        protected virtual void InitColumns()
        {
            Columns.Clear();
            Columns.AddColumn(CollectionObject.TitlePropertyId, 220, Constants.TitleKey);
        }

        private void lvCollection_ControlAdded(object sender, ControlEventArgs e)
        {
            InitColumns();

            lvCollection.Items.Clear();
            lvCollection.Columns.Clear();

            foreach (var column in Columns)
            {
                lvCollection.Columns.Add(column.Title, column.Width);
            }
        }
    }
}
