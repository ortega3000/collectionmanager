﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CollectionManager.Contracts.Interfaces;
using CollectionManager.Contracts.Types;
using CollectionManager.Contracts.Types.CollectionObjectTypes;
using CollectionManager.Contracts.Types.CollectionObjectTypes.TelephoneCard;

namespace CollectionManager.Contracts.Extensions
{
    public static class TelephoneCardExtensions
    {
        public static TelephoneCard FromBaseClass(this ICollectionObject objectToConvert)
        {
            if (objectToConvert == null)
            {
                throw new ArgumentNullException(nameof(objectToConvert));
            }

            var result = new TelephoneCard(objectToConvert);
            return result;
        }
    }
}
