﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using CollectionManager.Contracts.Interfaces;
using CollectionManager.Contracts.Types;
using CollectionManager.Contracts.Types.CollectionObjectTypes;
using MongoDB.Bson;
using MongoDB.Driver;

namespace CollectionManager.Contracts.Extensions
{
    public static class CollectionExtensions
    {
        public static string ConvertToString(this IMongoCollection<CollectionObject> collectionObject)
        {
            return "";
        }

        public static ICollectionObject ConvertToCollectionObject(this BsonDocument document)
        {
            var title = ExtractString(document, "Title", string.Empty);

            var result = new CollectionObject(new Guid(Constants.CollectionObjectTypeGuid))
            {
                Title = title
            };

            return result;
        }

        private static string ExtractString(BsonDocument document, string id, string defaultValue)
        {
            var item = document.Elements.FirstOrDefault(o => o.Name == id);
            if (item == null)
            {
                return defaultValue;
            }

            return item.Value.ToString();
        }
    }
}
