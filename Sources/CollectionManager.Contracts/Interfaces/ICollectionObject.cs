﻿using System;
using System.Collections.Generic;
using CollectionManager.Contracts.Types;

namespace CollectionManager.Contracts.Interfaces
{
    /// <summary>
    /// Initial skeleton of the collection object.
    /// Contains only basic and minimally required properties.
    /// </summary>
    public interface ICollectionObject
    {
        Guid ObjectId { get; set; }
        Guid ObjectType { get; set; }
        /// <summary>
        /// The title of the object
        /// </summary>
        string Title { get; set; }
        /// <summary>
        /// The description of the object
        /// </summary>
        string Description { get; set; }
        /// <summary>
        /// The date of addition of the object to the collection
        /// </summary>
        DateTime DateAdded { get; set; }
        /// <summary>
        /// Indicates whether the object is active
        /// </summary>
        bool Active { get; set; }
        /// <summary>
        /// Indicates whether this object is placed into the archive
        /// </summary>
        bool Archived { get; set; }

        PropertiesCollection Properties { get; set; }

        List<string> Tags { get; set; }

        void UpdateProperty<T>(Guid id, T value);
        void AddProperty(Property property);
        bool TryAddProperty(Guid parentId, Property property);
        void AddProperty<T>(Guid id, string title, PropertyType type, T value);
        T GetProperty<T>(Guid id, T defaultValue);
    }
}
