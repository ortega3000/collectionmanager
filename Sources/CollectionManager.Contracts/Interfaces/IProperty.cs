﻿using System;
using System.Collections.Generic;
using CollectionManager.Contracts.Types;

namespace CollectionManager.Contracts.Interfaces
{
    public interface IProperty
    {
        PropertyType Type { get; set; }

        Guid PropertyId { get; set; }
        string Title { get; set; }
        object Value { get; set; }
        PropertiesCollection SubProperties { get; set; }

        void AddSubProperty(Property property);
        void RemoveSubProperty(Guid id);
        void ClearSubProperties();
    }
}