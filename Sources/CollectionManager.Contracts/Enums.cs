﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionManager.Contracts
{
    public class Enums
    {
        public enum Country
        {
            Greece,
            Poland,
            Ukraine
        }

        public enum Currency
        {
            USD, 
            EUR,
            UAH
        }
    }
}
