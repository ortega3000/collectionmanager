﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CollectionManager.Database;
using CollectionManager.Properties;

namespace CollectionManager.Forms
{
    public partial class CollectionSelector : Form
    {
        public MongoDatabase Database { get; set; }
        public string SelectedCollectionName { get; set; }

        public CollectionSelector()
        {
            InitializeComponent();
        }

        private void listBoxCollections_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectedCollectionName = listBoxCollections.Text;
        }

        private void CollectionSelector_Load(object sender, EventArgs e)
        {
            var list = Database.ListCollectionNames();

            listBoxCollections.BeginUpdate();

            try
            {
                foreach (var dbName in list)
                {
                    listBoxCollections.Items.Add(dbName);
                }
            }
            finally
            {
                listBoxCollections.EndUpdate();
            }
        }
    }
}
