﻿namespace CollectionManager.Forms
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.lblDatabaseName = new System.Windows.Forms.Label();
            this.tbDatabaseName = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.tbDatabaseAddress = new System.Windows.Forms.TextBox();
            this.lblDatabaseAddress = new System.Windows.Forms.Label();
            this.btnDefaults = new System.Windows.Forms.Button();
            this.cbConnectAutomatically = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lblDatabaseName
            // 
            resources.ApplyResources(this.lblDatabaseName, "lblDatabaseName");
            this.lblDatabaseName.Name = "lblDatabaseName";
            // 
            // tbDatabaseName
            // 
            resources.ApplyResources(this.tbDatabaseName, "tbDatabaseName");
            this.tbDatabaseName.Name = "tbDatabaseName";
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            resources.ApplyResources(this.btnOK, "btnOK");
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Name = "btnOK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // tbDatabaseAddress
            // 
            resources.ApplyResources(this.tbDatabaseAddress, "tbDatabaseAddress");
            this.tbDatabaseAddress.Name = "tbDatabaseAddress";
            // 
            // lblDatabaseAddress
            // 
            resources.ApplyResources(this.lblDatabaseAddress, "lblDatabaseAddress");
            this.lblDatabaseAddress.Name = "lblDatabaseAddress";
            // 
            // btnDefaults
            // 
            resources.ApplyResources(this.btnDefaults, "btnDefaults");
            this.btnDefaults.Name = "btnDefaults";
            this.btnDefaults.UseVisualStyleBackColor = true;
            this.btnDefaults.Click += new System.EventHandler(this.btnDefaults_Click);
            // 
            // cbConnectAutomatically
            // 
            resources.ApplyResources(this.cbConnectAutomatically, "cbConnectAutomatically");
            this.cbConnectAutomatically.Name = "cbConnectAutomatically";
            this.cbConnectAutomatically.UseVisualStyleBackColor = true;
            // 
            // SettingsForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cbConnectAutomatically);
            this.Controls.Add(this.btnDefaults);
            this.Controls.Add(this.tbDatabaseAddress);
            this.Controls.Add(this.lblDatabaseAddress);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.tbDatabaseName);
            this.Controls.Add(this.lblDatabaseName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SettingsForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDatabaseName;
        private System.Windows.Forms.TextBox tbDatabaseName;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TextBox tbDatabaseAddress;
        private System.Windows.Forms.Label lblDatabaseAddress;
        private System.Windows.Forms.Button btnDefaults;
        private System.Windows.Forms.CheckBox cbConnectAutomatically;
    }
}