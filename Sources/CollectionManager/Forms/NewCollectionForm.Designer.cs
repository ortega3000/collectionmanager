﻿namespace CollectionManager.Forms
{
    partial class NewCollectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tbCollectionName = new System.Windows.Forms.TextBox();
            this.lblCollectionName = new System.Windows.Forms.Label();
            this.lblCollectionType = new System.Windows.Forms.Label();
            this.cbCollectionTypes = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnOK.Location = new System.Drawing.Point(404, 114);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCancel.Location = new System.Drawing.Point(485, 114);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // tbCollectionName
            // 
            this.tbCollectionName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCollectionName.Location = new System.Drawing.Point(120, 29);
            this.tbCollectionName.Name = "tbCollectionName";
            this.tbCollectionName.Size = new System.Drawing.Size(440, 20);
            this.tbCollectionName.TabIndex = 7;
            this.tbCollectionName.Text = "test";
            // 
            // lblCollectionName
            // 
            this.lblCollectionName.AutoSize = true;
            this.lblCollectionName.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCollectionName.Location = new System.Drawing.Point(16, 32);
            this.lblCollectionName.Name = "lblCollectionName";
            this.lblCollectionName.Size = new System.Drawing.Size(82, 13);
            this.lblCollectionName.TabIndex = 6;
            this.lblCollectionName.Text = "Collection name";
            // 
            // lblCollectionType
            // 
            this.lblCollectionType.AutoSize = true;
            this.lblCollectionType.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblCollectionType.Location = new System.Drawing.Point(16, 59);
            this.lblCollectionType.Name = "lblCollectionType";
            this.lblCollectionType.Size = new System.Drawing.Size(76, 13);
            this.lblCollectionType.TabIndex = 8;
            this.lblCollectionType.Text = "Collection type";
            // 
            // cbCollectionTypes
            // 
            this.cbCollectionTypes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCollectionTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCollectionTypes.FormattingEnabled = true;
            this.cbCollectionTypes.Items.AddRange(new object[] {
            "Telephone card"});
            this.cbCollectionTypes.Location = new System.Drawing.Point(120, 56);
            this.cbCollectionTypes.Name = "cbCollectionTypes";
            this.cbCollectionTypes.Size = new System.Drawing.Size(440, 21);
            this.cbCollectionTypes.TabIndex = 9;
            this.cbCollectionTypes.SelectedIndexChanged += new System.EventHandler(this.cbCollectionTypes_SelectedIndexChanged);
            // 
            // NewCollectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 149);
            this.Controls.Add(this.cbCollectionTypes);
            this.Controls.Add(this.lblCollectionType);
            this.Controls.Add(this.tbCollectionName);
            this.Controls.Add(this.lblCollectionName);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "NewCollectionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "New collection";
            this.Load += new System.EventHandler(this.NewCollectionForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox tbCollectionName;
        private System.Windows.Forms.Label lblCollectionName;
        private System.Windows.Forms.Label lblCollectionType;
        private System.Windows.Forms.ComboBox cbCollectionTypes;
    }
}