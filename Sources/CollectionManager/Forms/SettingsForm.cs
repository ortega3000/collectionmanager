﻿using System.Windows.Forms;
using CollectionManager.Properties;

namespace CollectionManager.Forms
{
    public partial class SettingsForm : Form
    {
        private const string DatabaseAddressDefaultValue = "mongodb://localhost";
        private const string DatabaseNameDefaultValue = "My Collection";

        public SettingsForm()
        {
            InitializeComponent();
        }

        public string DatabaseName
        {
            get => tbDatabaseName.Text;
            set => tbDatabaseName.Text = value;
        }

        public string DatabaseAddress
        {
            get => tbDatabaseAddress.Text;
            set => tbDatabaseAddress.Text = value;
        }

        public bool ConnectAutomatically
        {
            get => cbConnectAutomatically.Checked;
            set => cbConnectAutomatically.Checked = value;
        }

        private void btnDefaults_Click(object sender, System.EventArgs e)
        {
            if (MessageBox.Show( Resources.ResetToDefaultsMessage, "Confirm",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                tbDatabaseName.Text = DatabaseNameDefaultValue;
                tbDatabaseAddress.Text = DatabaseAddressDefaultValue;
                cbConnectAutomatically.Checked = false;
            }
        }
    }
}
