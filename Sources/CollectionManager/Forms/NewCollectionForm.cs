﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CollectionManager.Contracts.Types;
using CollectionManager.Database;

namespace CollectionManager.Forms
{
    public partial class NewCollectionForm : Form
    {
        public string NewCollectionName => tbCollectionName.Text;
        public CollectionTypes.CollectionTypeInfo NewCollectionType { get; set; }

        public NewCollectionForm()
        {
            InitializeComponent();
        }

        private void NewCollectionForm_Load(object sender, EventArgs e)
        {
            cbCollectionTypes.Items.Clear();

            foreach (var typeInfo in CollectionTypes.KnownCollections)
            {
                cbCollectionTypes.Items.Add(typeInfo.DisplayName);
            }
        }

        private void cbCollectionTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedType = cbCollectionTypes.Items[cbCollectionTypes.SelectedIndex].ToString();
            NewCollectionType = CollectionTypes.KnownCollections.FirstOrDefault(o => o.DisplayName == selectedType);
        }

    }
}
