﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CollectionManager.Database;

namespace CollectionManager.Controls
{
    public partial class OneCollection : UserControl
    {
        private MongoDatabase _database;
        private string _collectionName;

        public MongoDatabase Database
        {
            get => _database;
            set => _database = value;
        }

        public void OpenCollection(MongoDatabase database, string collectionName)
        {
            _database = database;
            _collectionName = collectionName;

        }

        public OneCollection()
        {
            InitializeComponent();
        }
    }
}
