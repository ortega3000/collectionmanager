﻿using CollectionManager.Contracts.GUI.PhoneCards;
using CollectionManager.Contracts.Interfaces;
using CollectionManager.Contracts.Types;
using CollectionManager.Contracts.Types.CollectionObjectTypes;
using CollectionManager.Contracts.Types.CollectionObjectTypes.TelephoneCard;
using CollectionManager.Core;
using CollectionManager.Database;
using CollectionManager.Forms;
using CollectionManager.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using CollectionManager.Contracts.GUI.Common;

namespace CollectionManager
{
    public partial class MainForm : Form
    {
        private readonly CancellationTokenSource _tokenSource;
        private MongoDatabase _database;

        public MainForm()
        {
            _tokenSource = new CancellationTokenSource();

            InitializeComponent();
        }

        private void mnuExit_Click(object sender, System.EventArgs e)
        {
            _tokenSource.Cancel();

            Close();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = false;
        }

        private void mnuSettings_Click(object sender, System.EventArgs e)
        {
            var settingsForm = new SettingsForm
            {
                DatabaseName = Settings.Default.DatabaseName,
                DatabaseAddress = Settings.Default.MongoDbServer,
                ConnectAutomatically = Settings.Default.ConnectAutomatically
            };

            var result = settingsForm.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                Settings.Default.MongoDbServer = settingsForm.DatabaseAddress;
                Settings.Default.DatabaseName = settingsForm.DatabaseName;
                Settings.Default.ConnectAutomatically = settingsForm.ConnectAutomatically;
            }
        }

        private void btnConnect_Click(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(Settings.Default.MongoDbServer))
            {
                MessageBox.Show(Resources.MisconfiguredSettingsMessage);
                return;
            }

            ConnectToDatabase();
        }

        private void ConnectToDatabase()
        {
            _database = new MongoDatabase(Settings.Default.MongoDbServer, _tokenSource.Token);
            _database.UseDatabase(Constants.MyDatabaseName);
        }

        private void MainForm_Load(object sender, System.EventArgs e)
        {
            /*
                        var newMenuItem = new ToolStripMenuItem("Telephone card")
                        {
                            Tag = typeof(TelephoneCard)
                        };
                        newMenuItem.Click += NewMenuItemOnClick;
                        mnuOpen.DropDownItems.Add(newMenuItem);
            */

            if (Settings.Default.ConnectAutomatically)
            {
                btnConnect_Click(this, EventArgs.Empty);
            }
        }

        private void NewMenuItemOnClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void mnuOpen_Click(object sender, EventArgs e)
        {
            CollectionTypes.CollectionTypeInfo desiredType = null;
            string desiredCollectionName = string.Empty;

            if (_database == null)
            {
                ConnectToDatabase();
            }

            if (!_database.HasCollections())
            {
                if (MessageBox.Show("You have no collections yet. Do you want to create the new one?", "Confirm",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }

                var newCollectionForm = new NewCollectionForm();
                if (newCollectionForm.ShowDialog(this) == DialogResult.OK)
                {
                    _database.CreateCollection(newCollectionForm.NewCollectionName);
                    desiredCollectionName = newCollectionForm.NewCollectionName;
                    desiredType = newCollectionForm.NewCollectionType;
                }
            }
            else
            {
                var form = new CollectionSelector { Database = _database };
                if (form.ShowDialog(this) != DialogResult.OK)
                {
                    return;
                }

                desiredCollectionName = form.SelectedCollectionName;
                desiredType = CollectionTypes.KnownCollections.FirstOrDefault();
            }

            OpenCollection(desiredType, desiredCollectionName);
        }

        private void OpenCollection(CollectionTypes.CollectionTypeInfo typeInfo, string collectionName)
        {
            CollectionObjectsList control;
            switch (typeInfo.CollectionType)
            {
                case CollectionTypes.CollectionType.PhoneCard:
                    control = new TelephoneCardsList
                    {
                        Dock = DockStyle.Fill
                    };
                    break;
                default:
                    MessageBox.Show($"Type of collection {typeInfo.CollectionType} not recognized.");
                    return;
            }

            var collection = _database.GetCollection(collectionName);
            control.Collection = collection;
            mainPanel.Controls.Add(control);
        }
    }
}
