﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImgSplitter
{
    public partial class ImageSelector : UserControl
    {
        public List<Image> Images;
        private string originalImageDirectory;
        private string originalImageFileName;

        public ImageSelector()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog
            {
                Filter = "Images|*.jpg",
                Multiselect = false
            };
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                originalImageFileName = Path.GetFileNameWithoutExtension(ofd.FileName);
                originalImageDirectory = Path.GetDirectoryName(ofd.FileName);
                pictureBox1.Load($"file://{ofd.FileName}");
            }
        }

        public void SplitImage()
        {
            var targetDirectory = Path.Combine(originalImageDirectory, originalImageFileName);
            if (Directory.Exists(targetDirectory))
            {
                Directory.Delete(targetDirectory, true);
            }

            Directory.CreateDirectory(targetDirectory);

            Images = new List<Image>();

            int h1 = 1225;
            var w1 = 1960;
            var w2 = 2003;
            var h2 = 1248;
            var dh = 30;
            var dw = 2005;
            var row2top = h1 + dh;
            var row2h = h2;
            var col2x1 = dw;
            var col2x2 = dw + w2;
            
            var allSizes = new List<Rectangle>
            {
                // row 1
                new Rectangle(0, 0, w1, h1),
                new Rectangle(col2x1, 0, w2, h1),
                // row 2
                new Rectangle(0, row2top, w1, h2),
                new Rectangle(col2x1, row2top, w2, h2),
                // row 3
                new Rectangle(0, row2top + (h2+dh), w1, h2),
                new Rectangle(col2x1, row2top + (h2+dh), w2, h2),
                // row 4
                new Rectangle(0, row2top + (h2+dh)*2, w1, h2),
                new Rectangle(col2x1, row2top + (h2+dh)*2, w2, h2),
                // row 5
                new Rectangle(0, row2top + (h2+dh)*3, w1, h2),
                new Rectangle(col2x1, row2top + (h2+dh)*3, w2, h2),
            };

            var bmp = pictureBox1.Image as Bitmap;
            if (bmp == null)
            {
                MessageBox.Show("Cant convert image to bitmap.");
                return;
            }

            var index = 1;
            for (var x=0; x<allSizes.Count;x++)
            {
                var rectangle = allSizes[x];
                var target = new Bitmap(rectangle.Width, rectangle.Height);
/*

                                using (var g = Graphics.FromImage(target))
                                {
                                    g.DrawImage(bmp, new Rectangle(0, 0, target.Width, target.Height),
                                        rectangle,
                                        GraphicsUnit.Pixel);
                                    g.Save();
                                }
                */
                var fn = Path.Combine(targetDirectory, $"file_{index}.bmp");
                if (bmp.Width < rectangle.Right)
                {
                    rectangle.Width = bmp.Width - rectangle.Left;
                }

                if (bmp.Height < rectangle.Bottom)
                {
                    rectangle.Height = bmp.Height - rectangle.Top;
                }

                var img = bmp.Clone(rectangle, target.PixelFormat);
                img.Save(fn);
                index++;
            }
        }

        private void btnSplit_Click(object sender, EventArgs e)
        {
            SplitImage();
        }
    }
}
