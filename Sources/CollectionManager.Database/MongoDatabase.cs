﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CollectionManager.Contracts.Extensions;
using CollectionManager.Contracts.Interfaces;
using CollectionManager.Contracts.Types;
using CollectionManager.Contracts.Types.CollectionObjectTypes;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace CollectionManager.Database
{
    public sealed class MongoDatabase
    {
        private MongoClient _client;
        private readonly string _connectionString;
        private string _databaseName;
        private readonly CancellationToken _token;
        private IMongoDatabase _database;

        public MongoDatabase(string connectionString, CancellationToken token)
        {
            _connectionString = connectionString;
            _token = token;

            Debug.Assert(!string.IsNullOrEmpty(_connectionString));

            _client = new MongoClient(_connectionString);
        }

        public List<string> ListDatabases()
        {
            var awaiter = _listDatabases().GetAwaiter();
            return awaiter.GetResult();
        }

        private async Task<ListOfStrings> _listDatabases()
        {
            var list = new ListOfStrings();

            using (var cursor = _client.ListDatabaseNames(_token))
            {
                var dbs = await cursor.ToListAsync(cancellationToken: _token);
                foreach (var db in dbs)
                {
                    list.Add(db);
                }
            }

            return list;
        }

        public List<string> ListCollectionNames()
        {
            using (var collCursor = _database.ListCollectionNames())
            {
                var collections = collCursor.ToList();
                return collections;
            }
        }

        public bool HasCollections()
        {
            using (var collCursor = _database.ListCollectionNames())
            {
                var collections = collCursor.ToList();
                return collections.Any();
            }
        }

        public void UseDatabase(string databaseName)
        {
            _databaseName = databaseName;
            _database = _client.GetDatabase(_databaseName);
        }

        public void DropDatabase(string databaseName)
        {
            if (_databaseName == databaseName.ToLowerInvariant())
            {
                throw new ArgumentException($"Database {databaseName} is in use.");
            }
            _client.DropDatabase(databaseName, _token);
            _database = null;
        }

        private async Task InitDatabase()
        {
            using (var cursor = await _client.ListDatabasesAsync(_token))
            {
                var dbs = await cursor.ToListAsync(cancellationToken: _token);
                foreach (var db in dbs)
                {
                    var dbName = db["name"].ToString().ToLowerInvariant();
                    if (dbName != _databaseName)
                    {
                        continue;
                    }
                    _database = _client.GetDatabase(_databaseName);
                    return;
                }
            }

            // database was not found
            _database = _client.GetDatabase(_databaseName);
        }

        public bool DoesCollectionExist(string collectionName)
        {
            using (var collCursor = _database.ListCollectionNames())
            {
                var collections = collCursor.ToList();
                return collections.Any(o => string.Equals(o, collectionName, StringComparison.InvariantCultureIgnoreCase));
            }
        }

        public bool CreateCollection(string collectionName)
        {
            if (DoesCollectionExist(collectionName))
            {
                return true;
            }
            _database.CreateCollection(collectionName, cancellationToken: _token);
            return DoesCollectionExist(collectionName);
        }

        public bool DropCollection(string collectionName)
        {
            if (!DoesCollectionExist(collectionName))
            {
                return true;
            }

            _database.DropCollection(collectionName, _token);

            return !DoesCollectionExist(collectionName);
        }

        public string ExportCollection(string collectionName)
        {
            CheckCollectionExistenceOrThrow(collectionName);

            var collection = _database.GetCollection<CollectionObject>(collectionName);
            return "";
        }

        private void CheckCollectionExistenceOrThrow(string collectionName)
        {
            if (!DoesCollectionExist(collectionName))
            {
                throw new DatabaseException($"Database {collectionName} does not exist.");
            }
        }

        public bool AddItemToCollection(string collectionName, ICollectionObject item)
        {
            var collection = _database.GetCollection<BsonDocument>(collectionName);
            item.DateAdded = DateTime.UtcNow;
            var bsonItem = item.ToBsonDocument();
            collection.InsertOne(bsonItem, cancellationToken: _token);

            return true;
        }

        public bool TryFindItemById(string collectionName, Guid id, out ICollectionObject result)
        {
            result = null;

            var collection = _database.GetCollection<CollectionObject>(collectionName);
            var filter = new BsonDocument("ObjectId", id);
            var task = collection.Find(filter).ToListAsync(cancellationToken: _token).GetAwaiter();
            var list = task.GetResult();
            if (list != null && list.Any())
            {
                result = list.FirstOrDefault();//.ConvertToCollectionObject();
                return true;
            }

            return false;
        }

        public bool TryDropItemById(string collectionName, Guid id)
        {
            var collection = _database.GetCollection<BsonDocument>(collectionName);
            var filter = new BsonDocument("ObjectId", id);
            var task = collection.Find(filter).ToListAsync(cancellationToken: _token).GetAwaiter();
            var list = task.GetResult();
            if (list != null && list.Any())
            {
                collection.DeleteOne(filter, _token);
                return true;
            }

            return false;
        }

        public bool UpdateItem(string collectionName, ICollectionObject item)
        {
            var collection = _database.GetCollection<BsonDocument>(collectionName);
            var filter = new BsonDocument("ObjectId", item.ObjectId);
            var task = collection.ReplaceOne(filter, item.ToBsonDocument());

            return task.IsAcknowledged;
        }

        public List<ICollectionObject> GetCollection(string collectionName)
        {
            return new List<ICollectionObject>();
        }
    }

    public class ListOfStrings : List<string>
    {
    }
}
