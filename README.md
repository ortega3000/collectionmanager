## The story

The project was started from scratch in an attempt to handle the collection of plastic cards.
The source codes are open to everyone willing to know how the applications of this kind do their work.
You are free to open discussions regarding the functions of the application.
Also, you can commit your changes (if any) using pull requests.

---

## Is this project open-source?

Yes, it is. You are free to browse, grab and re-use it partially or complete in your personal or non-commercial software.
Any commercial usage of this source codes is prohibited. 

---
